python-firehose (0.5-3) unstable; urgency=medium

  * Team upload.
  * Add missing runtime dependency on python3-six
    (See firehose/model.py)

 -- Alexandre Detiste <tchet@debian.org>  Thu, 29 Aug 2024 08:30:19 +0200

python-firehose (0.5-2) unstable; urgency=medium

  * Team upload.
  * Replace Nose with Pytest.

 -- Alexandre Detiste <tchet@debian.org>  Wed, 07 Aug 2024 12:57:46 +0200

python-firehose (0.5-1) unstable; urgency=medium

  * Upload to unstable
  * Add myself as uploader, with permission from paultag
  * New upstream version: 0.5
  * Drop fix-python3.patch: Applied upstream
  * Add fix-findbugs-test.patch
    - Renames a utility function so it is no longer executed
      like a test.

 -- Matthias Klumpp <mak@debian.org>  Thu, 04 Jan 2024 10:02:52 +0100

python-firehose (0.3-4) experimental; urgency=medium

  * Team upload.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Boyuan Yang <byang@debian.org>  Fri, 01 Jul 2022 13:21:40 -0400

python-firehose (0.3-3) experimental; urgency=medium

  * Team Upload.
  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Nilesh Patra ]
  * Replace plistlib deprecated calls
    (Closes: #973747)
  * Switch watch version to 4
  * Bump debhelper-compat version to 13
  * Declare compliance with policy 4.5.1
  * Add "Rules-Requires-Root:no"
  * Add upstream/metadata
  * Minor fix in description

 -- Nilesh Patra <npatra974@gmail.com>  Sun, 27 Dec 2020 11:44:11 +0530

python-firehose (0.3-2) experimental; urgency=medium

  * Team upload.
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Fix Format URL to correct one
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Use pybuild to build package.
  * Bump debhelper compat level to 12.
  * Enable autopkgtest-pkg-python testsuite.
  * d/watch: Use pypi.debian.net.
  * Bump standards version to 4.4.0.
  * Fix Python 3 support and run tests during build.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Fri, 13 Sep 2019 08:35:16 +0200

python-firehose (0.3-1) experimental; urgency=low

  * Upstream release
  * Add debian/test-data/findbugs_jformatstring.txt - to be removed for 0.4,
    upstrem was missing it in the MANIFEST.in

 -- Paul Tagliamonte <paultag@debian.org>  Tue, 08 Oct 2013 21:14:22 -0400

python-firehose (0.2-1) experimental; urgency=low

  * Initial release. (To experimental because the Schema is likely to change
    and I don't want it in testing yet)

 -- Paul Tagliamonte <paultag@debian.org>  Wed, 14 Aug 2013 01:10:13 -0400
